# Horsen

Morsen für Ganoven.

## Bauen

Abhängigkeiten:
- GNU Make
- `gcc`

Kompilieren:
```bash
make build
```

## Ausführen

```bash
$ ./horsen_enc Guten Tag.
Original: Guten Tag.
Encoded (Morse): --. ..- - . -. / - .- --. .-.-.-
Encoded (Horsen): mmh hhm m h mh / m hm mmh hmhmhm
Encoded (Wrong): hhm mmh h m hm / h mh hhm mhmhmh

$ ./horsen_dec mmh hhm m h mh / m hm mmh hmhmhm
Original: mmh hhm m h mh / m hm mmh hmhmhm
Decoded (Horsen): GUTEN TAG.
Decoded (Wrong): UGETA ENU;
```

