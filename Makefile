CFLAGS=-Wall -Werror

.PHONY: default
default: build

.PHONY: build
build: horsen

.PHONY: clean
clean:
	$(RM) horsen
