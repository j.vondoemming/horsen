#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define ALPHABETSIZE 50
#define MAXMORSELEN 10

const char alphabet[ALPHABETSIZE][2][MAXMORSELEN + 1] = {
	{"A", ".-"},
	{"B", "-..."},
	{"C", "-.-."},
	{"D", "-.."},
	{"E", "."},
	{"F", "..-."},
	{"G", "--."},
	{"H", "...."},
	{"I", ".."},
	{"J", ".---"},
	{"K", "-.-"},
	{"L", ".-.."},
	{"M", "--"},
	{"N", "-."},
	{"O", "---"},
	{"P", ".--."},
	{"Q", "--.-"},
	{"R", ".-."},
	{"S", "..."},
	{"T", "-"},
	{"U", "..-"},
	{"V", "...-"},
	{"W", ".--"},
	{"X", "-..-"},
	{"Y", "-.--"},
	{"Z", "--.."},
	{"0", "-----"},
	{"1", ".----"},
	{"2", "..---"},
	{"3", "...--"},
	{"4", "....-"},
	{"5", "....."},
	{"6", "-...."},
	{"7", "--..."},
	{"8", "---.."},
	{"9", "----."},
	{".", ".-.-.-"},
	{"?", "..--.."},
	{";", "-.-.-."},
	{":", "---..."},
	{"/", "-..-."},
	{"-", "-....-"},
	{"'", ".----."},
	{"\"", ".-..-."},
	{"(", "-.--."},
	{")", "-.--.-"},
	{"=", "-...-"},
	{"+", ".-.-."},
	{"@", ".--.-."},
	{" ", "/"}
};

const char * encode_single(char c) {
	int i;
	for (i = 0; i < ALPHABETSIZE; i++) {
		if (toupper(c) == alphabet[i][0][0])
			return alphabet[i][1];
	}
	return NULL;
}

/**
 * Encodes a given string to morse code.
 *
 * @returns The new string containing the morse code.
 */
char * encode(const char *inp) {
	if (inp == NULL)
		return NULL;
	char *out = malloc(sizeof(char) * (strlen(inp) * MAXMORSELEN + 1));
	if (out == NULL)
		return NULL;
	char *end = out;
	const char *cur = inp;
	while (*cur != '\0') {
		const char *enc = encode_single(*cur);
		if (enc != NULL) {
			// copy the morse code to the end of out
			strcpy(end, enc);
			end+=strlen(enc);
			*end = ' ';
			end++;
		} else {
			// copy the unknown character to the end of out
			*end = *cur;
			end++;
			/**end = ' ';
			end++;*/
		}
		cur++;
	}
	*end = '\0';
	return out;
}

char decode_single(const char * morse) {
	char buf[MAXMORSELEN + 1];
	int i;
	for (i = 0; i<MAXMORSELEN; i++) {
		char c = (buf[i] = morse[i]);
		if (!(c == '-' || c == '.' || c == '/'))
			break;
	}
	buf[i] = '\0';
	for (i = 0; i < ALPHABETSIZE; i++) {
		if (strcmp(buf, alphabet[i][1]) == 0) {
			return alphabet[i][0][0];
		}
	}
	return '\0';
}

/**
 * Decodes a given morse code string to normal text.
 *
 * @returns The new string containing the normal text.
 */
char * decode(const char *inp) {
	if (inp == NULL)
		return NULL;
	char *out = malloc(sizeof(char) * (strlen(inp) + 1));
	if (out == NULL)
		return NULL;
	char *end = out;
	const char *cur = inp;
	while (*cur != '\0') {
		char dec = decode_single(cur);
		if (dec != '\0') {
			// copy the decoded character to the end of out
			*end = dec;
			end++;
			cur+=strlen(encode_single(dec));
		} else {
			// copy the unknown character to the end of out
			*end = *cur;
			end++;
		}
		if (*cur == '\0')
			break;
		cur++;
	}
	*end = '\0';
	return out;
}

/**
 * Similar to the tr command.
 */
void translate(char *str, const char *from, const char *to) {
	if (str == NULL)
		return;
	char *cur = str;
	while (*cur != '\0') {
		char *found = strchr(from, *cur);
		if (found != NULL)
			*cur = to[found-from];
		cur++;
	}
}

void print_usage(const char * progname) {
	fprintf(stderr, "Usage: %s <e|d> <dot replacement> <dash replacement>\n", progname);
}

int main(int argc, char **argv) {
	if (argc != 4) {
		print_usage(argv[0]);
		return -1;
	}
	char ed = argv[1][0];
	if (ed != 'e' && ed != 'd') {
		print_usage(argv[0]);
		return -1;
	}
	char tr1[3];
	tr1[0] = argv[2][0];
	tr1[1] = argv[3][0];
	tr1[2] = '\0';
	const char tr2[3] = ".-";

	char buf[4096];
	char *res = NULL;
	while (fgets(buf, 4096, stdin) != NULL) {
		switch (ed) {
			case 'e':
				res = encode(buf);
				translate(res, tr2, tr1);
				break;
			case 'd':
				translate(buf, tr1, tr2);
				res = decode(buf);
				break;
		}
		puts(res);
		free(res);
	}

	return 0;
}
